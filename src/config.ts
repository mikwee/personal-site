// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = "mikwee's blog";
export const SITE_DESCRIPTION =
  "Just another random blog by a Tel Avivi.";
export const TWITTER_HANDLE = "@toon_mikwee";
export const MY_NAME = "mikwee";

// setup in astro.config.mjs
const BASE_URL = new URL(import.meta.env.SITE);
export const SITE_URL = BASE_URL.origin;
