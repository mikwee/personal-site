Hi! As much as I love Ruby, I guess Jekyll is kind of outdated, so I decided to give my personal site a makeover using [Astro](https://astro.build), a build tool that can, at its most bare-bones form, be used as a static site generator. To do so, I enlisted the help of [Blogster Bubblegum](https://github.com/flexdinesh/blogster#bubblegum), a blog-oriented theme, and don't you think it just looks beautiful?

My old site will always be available at [mikwee/personal-site-jekyll](https://codeberg.org/mikwee/personal-site-jekyll) on Codeberg.

<div style="display: flex; margin: 0 auto;">
    <img src="./images/../public/images/CC-BY.png" alt="CC-BY logo" href="https://creativecommons.org/licenses/by/4.0/" style="padding-right: 1rem; height: 50%"/>
    <img src="https://upload.wikimedia.org/wikipedia/commons/7/78/Definition_of_Free_Cultural_Works_logo_notext.svg" alt="Free Content logo" href="https://freedomdefined.org/Definition/1.1" style="height: 50%"/>
</div>

The site's code is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html), while the site's content is licensed under the [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license.